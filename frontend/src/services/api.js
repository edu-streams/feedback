import axios from "axios";

// export const backend = axios.create({
//   baseURL: "http://localhost:8080",
// });

export const backend = axios.create({
    baseURL: "https://stage.edu-streams.org" || "http://localhost:8080",
});

export const postFeedBack = async (body) => {
    try {
        const { data } = await backend.post("/feedback/api", body);
        return data;
    } catch (error) {
        return error;
    }
};

export const getFeedBack = async () => {
    try {
        const { data } = await backend.get("/feedback/api");
        return data;
    } catch (error) {
        return error;
    }
};
