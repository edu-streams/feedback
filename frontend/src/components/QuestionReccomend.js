import { Box, InputLabel, FormControl, Input } from "@mui/material";
import { styled } from "@mui/system";
import CreateOutlinedIcon from "@mui/icons-material/CreateOutlined";
import { Icon } from "@iconify/react";

export const QuestionReccomend = ({ handleInputChange, isDisplay }) => {
    return (
        <QuestionBox>
            <FormControl sx={{ m: 1 }} variant="standard">
                <CustomInputLabel htmlFor="standard-adornment-amount">
                    Текст рекомендації
                </CustomInputLabel>

                <CustomInput
                    name="recommendQuestion"
                    onChange={handleInputChange}
                    id="standard-adornment-amount"
                    defaultValue={isDisplay.recommendQuestion}
                    multiline
                    endAdornment={
                        <CreateOutlinedIcon
                            position="end"
                            style={{ color: "rgba(0, 0, 0, 0.54)" }}
                        />
                    }
                />
            </FormControl>

            <RateBox>
                <Icon
                    icon="material-symbols:counter-1-rounded"
                    color="#991b1b"
                    width="24"
                    height="24"
                />
                <Icon
                    icon="material-symbols:counter-2"
                    color="#b91c1c"
                    width="24"
                    height="24"
                />
                <Icon
                    icon="material-symbols:counter-3"
                    color="#dc2626"
                    width="24"
                    height="24"
                />
                <Icon
                    icon="material-symbols:counter-4-rounded"
                    color="#ef4444"
                    width="24"
                    height="24"
                />
                <Icon
                    icon="material-symbols:counter-5-rounded"
                    color="#f87171"
                    width="24"
                    height="24"
                />
                <Icon
                    icon="material-symbols:counter-6"
                    color="#fca5a5"
                    width="24"
                    height="24"
                />
                <Icon
                    icon="material-symbols:counter-7-rounded"
                    color="#fb923c"
                    width="24"
                    height="24"
                />
                <Icon
                    icon="material-symbols:counter-8"
                    color="#f97316"
                    width="24"
                    height="24"
                />
                <Icon
                    icon="material-symbols:counter-9-rounded"
                    color="#22c55e"
                    width="24"
                    height="24"
                />
                <Icon
                    icon="mdi:numeric-10-circle"
                    color="#15803d"
                    width="24"
                    height="24"
                />
            </RateBox>
        </QuestionBox>
    );
};

const QuestionBox = styled(Box)`
    margin-bottom: 16px;
    background: #f1f5f9;
    border: 1px solid #d4d7db;
    border-radius: 4px;
`;

const CustomInput = styled(Input)`
    width: 315px;
    font-size: 14px;
`;

const CustomInputLabel = styled(InputLabel)`
    font-size: 12px;
    line-height: calc(12 / 12);
    align-items: center;
    letter-spacing: 0.15px;
    color: rgba(0, 0, 0, 0.6);
`;

const RateBox = styled(Box)`
    padding: 7px 0;
    display: flex;
    justify-content: center;
    gap: 8px;
`;
