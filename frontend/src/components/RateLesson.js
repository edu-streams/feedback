import * as React from "react";
import {
    Radio,
    RadioGroup,
    FormControl,
    FormLabel,
    Typography,
    Box,
} from "@mui/material";
import { styled } from "@mui/system";
import { Icon } from "@iconify/react";

export const RateLesson = ({ handleInputChange, formData, setingForm }) => {
    return (
        <Box>
            <CustomFormControl>
                <CustomTypography id="rate" variant="h6" gutterBottom>
                    {setingForm.question}
                    <span style={{ color: "red" }}>*</span>
                </CustomTypography>

                <CustomRadioGroup
                    onChange={handleInputChange}
                    value={formData.rate}
                    aria-labelledby="rate"
                    name="rate"
                >
                    <FormLabel>
                        <CustomRadio value="1" />
                        <CustomBox className="custom-radio-button">
                            {setingForm.isSmile === "smile" ? (
                                <Icon
                                    icon="ph:smiley-sad-fill"
                                    color="#BB0202"
                                    width="56"
                                    height="56"
                                />
                            ) : (
                                <Icon
                                    icon="material-symbols:counter-1-rounded"
                                    color="#BB0202"
                                    width="56"
                                    height="56"
                                />
                            )}
                        </CustomBox>
                    </FormLabel>

                    <FormLabel>
                        <CustomRadio value="2" />
                        <CustomBox className="custom-radio-button">
                            {setingForm.isSmile === "smile" ? (
                                <Icon
                                    icon="solar:smile-circle-bold"
                                    color="#F37908"
                                    width="56"
                                    height="56"
                                />
                            ) : (
                                <Icon
                                    icon="material-symbols:counter-2"
                                    color="#F37908"
                                    width="56"
                                    height="56"
                                />
                            )}
                        </CustomBox>
                    </FormLabel>

                    <FormLabel>
                        <CustomRadio value="3" />
                        <CustomBox className="custom-radio-button">
                            {setingForm.isSmile === "smile" ? (
                                <Icon
                                    icon="mdi:emoticon-neutral"
                                    color="#FFD800"
                                    width="56"
                                    height="56"
                                />
                            ) : (
                                <Icon
                                    icon="material-symbols:counter-3"
                                    color="#FFD800"
                                    width="56"
                                    height="56"
                                />
                            )}
                        </CustomBox>
                    </FormLabel>

                    <FormLabel>
                        <CustomRadio value="4" />
                        <CustomBox className="custom-radio-button">
                            {setingForm.isSmile === "smile" ? (
                                <Icon
                                    icon="solar:smile-circle-bold"
                                    color="#4BC820"
                                    width="56"
                                    height="56"
                                />
                            ) : (
                                <Icon
                                    icon="material-symbols:counter-4-rounded"
                                    color="#4BC820"
                                    width="56"
                                    height="56"
                                />
                            )}
                        </CustomBox>
                    </FormLabel>

                    <FormLabel>
                        <CustomRadio value="5" />
                        <CustomBox className="custom-radio-button">
                            {setingForm.isSmile === "smile" ? (
                                <Icon
                                    icon="ph:smiley-fill"
                                    color="#088B0D"
                                    width="56"
                                    height="56"
                                />
                            ) : (
                                <Icon
                                    icon="material-symbols:counter-5-rounded"
                                    color="#088B0D"
                                    width="56"
                                    height="56"
                                />
                            )}
                        </CustomBox>
                    </FormLabel>
                </CustomRadioGroup>
            </CustomFormControl>
        </Box>
    );
};

const CustomTypography = styled(Typography)`
    margin-bottom: 12px;
    font-weight: 400;
    font-size: 20px;
    line-height: calc(32 / 20);
    text-align: center;
    letter-spacing: 0.15px;
    color: rgba(0, 0, 0, 0.87);
`;

const CustomBox = styled(Box)`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 56px;
    height: 56px;
    cursor: pointer;

    & svg {
        transition: transform 250ms cubic-bezier(0.4, 0, 0.2, 1);
    }
    & svg:hover,
    & svg:focus {
        transform: scale(1.3);
    }
`;

const CustomFormControl = styled(FormControl)`
    margin-bottom: 18px;
    width: 100%;
`;

const CustomRadioGroup = styled(RadioGroup)`
    display: flex;
    flex-direction: row;
    justify-content: center;
    gap: 18px;
`;

const CustomRadio = styled(Radio)`
    display: none;

    &.Mui-checked + .custom-radio-button svg {
        transform: scale(1.3);
    }
`;
