import { Box, Tabs, Tab, Typography } from "@mui/material";
import { styled } from "@mui/system";
import MoodBadIcon from "@mui/icons-material/MoodBad";
import SentimentDissatisfiedIcon from "@mui/icons-material/SentimentDissatisfied";
import SentimentVeryDissatisfiedIcon from "@mui/icons-material/SentimentVeryDissatisfied";
import SentimentSatisfiedAltIcon from "@mui/icons-material/SentimentSatisfiedAlt";
import SentimentVerySatisfiedOutlinedIcon from "@mui/icons-material/SentimentVerySatisfiedOutlined";
import LooksOneOutlinedIcon from "@mui/icons-material/LooksOneOutlined";
import LooksTwoOutlinedIcon from "@mui/icons-material/LooksTwoOutlined";
import Looks3OutlinedIcon from "@mui/icons-material/Looks3Outlined";
import Looks4OutlinedIcon from "@mui/icons-material/Looks4Outlined";
import Looks5OutlinedIcon from "@mui/icons-material/Looks5Outlined";

export const ChooseFormRate = ({ chooseRate, isDisplay }) => {
    return (
        <QuestionBox>
            <RateQuestion>Оберіть форму оцінювання</RateQuestion>

            {isDisplay.isSmile === "smile" ? (
                <SmileBox>
                    <MoodBadIcon />
                    <SentimentVeryDissatisfiedIcon />
                    <SentimentDissatisfiedIcon />
                    <SentimentSatisfiedAltIcon />
                    <SentimentVerySatisfiedOutlinedIcon />
                </SmileBox>
            ) : (
                <SmileBox>
                    <LooksOneOutlinedIcon />
                    <LooksTwoOutlinedIcon />
                    <Looks3OutlinedIcon />
                    <Looks4OutlinedIcon />
                    <Looks5OutlinedIcon />
                </SmileBox>
            )}

            <Tabs
                value={isDisplay.isSmile}
                onChange={chooseRate}
                aria-label="wrapped label tabs example"
            >
                <Tab
                    value="smile"
                    label="Смайли"
                    style={{
                        textTransform: "none",
                        width: 50 + "%",
                        padding: 4 + "px",
                        minHeight: 48 + "px",
                    }}
                    icon={
                        <SentimentDissatisfiedIcon
                            style={{ marginBottom: 0 }}
                        />
                    }
                />
                <Tab
                    value="number"
                    label="Цифри"
                    style={{
                        textTransform: "none",
                        width: 50 + "%",
                        padding: 4 + "px",
                        minHeight: 48 + "px",
                    }}
                    icon={<Looks5OutlinedIcon style={{ marginBottom: 0 }} />}
                />
            </Tabs>
        </QuestionBox>
    );
};

const QuestionBox = styled(Box)`
    margin-bottom: 16px;
    background: #f1f5f9;
    border: 1px solid #d4d7db;
    border-radius: 4px;
`;

const RateQuestion = styled(Typography)`
    margin: 8px 0 0 8px;
    font-size: 12px;
    line-height: calc(12 / 12);
    align-items: center;
    letter-spacing: 0.15px;
    color: rgba(0, 0, 0, 0.6);
`;

const SmileBox = styled(Box)`
    padding: 12px 0 10px 0;
    display: flex;
    justify-content: center;
    gap: 22px;
    position: relative;

    &::after {
        content: "";
        display: block;
        width: 100%;
        height: 2px;
        position: absolute;
        bottom: 0;
        left: 0;
        background-color: rgba(224, 224, 224, 1);
    }
`;
