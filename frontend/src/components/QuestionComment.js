import { Box, InputLabel, FormControl, Input } from "@mui/material";
import CreateOutlinedIcon from "@mui/icons-material/CreateOutlined";
import { styled } from "@mui/system";

export const QuestionComment = ({ handleInputChange, isDisplay }) => {
    return (
        <QuestionBox>
            <FormControl sx={{ m: 1 }} variant="standard">
                <CustomInputLabel htmlFor="standard-adornment-amount">
                    Текст зворотьного зв’язку
                </CustomInputLabel>

                <CustomInput
                    name="commentQuestion"
                    onChange={handleInputChange}
                    id="standard-adornment-amount"
                    defaultValue={isDisplay.commentQuestion}
                    multiline
                    endAdornment={
                        <CreateOutlinedIcon
                            position="end"
                            style={{ color: "rgba(0, 0, 0, 0.54)" }}
                        />
                    }
                />
            </FormControl>
        </QuestionBox>
    );
};

const QuestionBox = styled(Box)`
    margin-bottom: 16px;
    background: #f1f5f9;
    border: 1px solid #d4d7db;
    border-radius: 4px;
`;

const CustomInput = styled(Input)`
    width: 315px;
    font-size: 14px;
`;

const CustomInputLabel = styled(InputLabel)`
    font-size: 12px;
    line-height: calc(12 / 12);
    align-items: center;
    letter-spacing: 0.15px;
    color: rgba(0, 0, 0, 0.6);
`;
