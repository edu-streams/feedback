import { Button } from "@mui/material";

export const SavedButton = ({ setShowSettings, showSettings }) => {
    return (
        <Button
            variant="outlined"
            style={{ color: "#2196F3", width: 100 + "%" }}
            onClick={() => {
                setShowSettings(!showSettings);
            }}
        >
            Зберегти
        </Button>
    );
};
