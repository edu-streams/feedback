import Button from "@mui/base/Button";
import { styled } from "@mui/system";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";

export const CloseButton = ({ closeForm }) => {
    return (
        <CustomButton onClick={closeForm}>
            <CustomCloseRoundedIcon width="32px" height="32px" />
        </CustomButton>
    );
};

const CustomButton = styled(Button)`
    width: 32px;
    height: 32px;
    position: absolute;
    top: 9.5px;
    right: 12px;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: transparent;
    cursor: pointer;
    border: none;
    z-index: 10;
`;

const CustomCloseRoundedIcon = styled(CloseRoundedIcon)`
    width: 32px;
    height: 32px;
    fill: rgba(0, 0, 0, 0.54);
`;
