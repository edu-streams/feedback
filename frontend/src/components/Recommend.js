import * as React from "react";
import {
    Radio,
    RadioGroup,
    FormControl,
    FormLabel,
    Typography,
    Box,
} from "@mui/material";
import { styled } from "@mui/system";
import { Icon } from "@iconify/react";

export const Recommend = ({ handleInputChange, formData, setingForm }) => {
    return (
        <Box>
            <CustomFormControl>
                <CustomTypography id="recommend" variant="h6" gutterBottom>
                    {setingForm["recommendQuestion"]}
                </CustomTypography>

                <CustomRadioGroup
                    onChange={handleInputChange}
                    value={formData.recommend}
                    aria-labelledby="recommend"
                    name="recommend"
                >
                    <FormLabel>
                        <CustomRadio value="1" />
                        <CustomBox className="custom-radio-button">
                            <Icon
                                icon="material-symbols:counter-1-rounded"
                                color="#991b1b"
                                width="52"
                                height="52"
                            />
                        </CustomBox>
                    </FormLabel>

                    <FormLabel>
                        <CustomRadio value="2" />
                        <CustomBox className="custom-radio-button">
                            <Icon
                                icon="material-symbols:counter-2"
                                color="#b91c1c"
                                width="52"
                                height="52"
                            />
                        </CustomBox>
                    </FormLabel>

                    <FormLabel>
                        <CustomRadio value="3" />
                        <CustomBox className="custom-radio-button">
                            <Icon
                                icon="material-symbols:counter-3"
                                color="#dc2626"
                                width="52"
                                height="52"
                            />
                        </CustomBox>
                    </FormLabel>

                    <FormLabel>
                        <CustomRadio value="4" />
                        <CustomBox className="custom-radio-button">
                            <Icon
                                icon="material-symbols:counter-4-rounded"
                                color="#ef4444"
                                width="52"
                                height="52"
                            />
                        </CustomBox>
                    </FormLabel>

                    <FormLabel>
                        <CustomRadio value="5" />
                        <CustomBox className="custom-radio-button">
                            <Icon
                                icon="material-symbols:counter-5-rounded"
                                color="#f87171"
                                width="52"
                                height="52"
                            />
                        </CustomBox>
                    </FormLabel>

                    <FormLabel>
                        <CustomRadio value="6" />
                        <CustomBox className="custom-radio-button">
                            <Icon
                                icon="material-symbols:counter-6"
                                color="#fca5a5"
                                width="52"
                                height="52"
                            />
                        </CustomBox>
                    </FormLabel>

                    <FormLabel>
                        <CustomRadio value="7" />
                        <CustomBox className="custom-radio-button">
                            <Icon
                                icon="material-symbols:counter-7-rounded"
                                color="#fb923c"
                                width="52"
                                height="52"
                            />
                        </CustomBox>
                    </FormLabel>

                    <FormLabel>
                        <CustomRadio value="8" />
                        <CustomBox className="custom-radio-button">
                            <Icon
                                icon="material-symbols:counter-8"
                                color="#f97316"
                                width="52"
                                height="52"
                            />
                        </CustomBox>
                    </FormLabel>

                    <FormLabel>
                        <CustomRadio value="9" />
                        <CustomBox className="custom-radio-button">
                            <Icon
                                icon="material-symbols:counter-9-rounded"
                                color="#22c55e"
                                width="52"
                                height="52"
                            />
                        </CustomBox>
                    </FormLabel>

                    <FormLabel>
                        <CustomRadio value="10" />
                        <CustomBox className="custom-radio-button">
                            <Icon
                                icon="mdi:numeric-10-circle"
                                color="#15803d"
                                width="52"
                                height="52"
                            />
                        </CustomBox>
                    </FormLabel>
                </CustomRadioGroup>
            </CustomFormControl>
        </Box>
    );
};

const CustomBox = styled(Box)`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 52px;
    height: 52px;
    cursor: pointer;

    & svg {
        transition: transform 250ms cubic-bezier(0.4, 0, 0.2, 1);
    }
    & svg:hover,
    & svg:focus {
        transform: scale(1.3);
    }
`;

const CustomFormControl = styled(FormControl)`
    margin-bottom: 18px;
    width: 100%;
`;

const CustomTypography = styled(Typography)`
    margin-bottom: 12px;
    font-weight: 400;
    font-size: 20px;
    line-height: calc(32 / 20);
    text-align: center;
    letter-spacing: 0.15px;
    color: rgba(0, 0, 0, 0.87);
`;

const CustomRadio = styled(Radio)`
    display: none;

    &.Mui-checked + .custom-radio-button svg {
        transform: scale(1.3);
    }
`;

const CustomRadioGroup = styled(RadioGroup)`
    display: flex;
    flex-direction: row;
    justify-content: center;
    gap: 12px;
`;
