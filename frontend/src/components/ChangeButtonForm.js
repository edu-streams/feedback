import { FormControl, Input } from "@mui/material";
import CreateOutlinedIcon from "@mui/icons-material/CreateOutlined";
import { styled } from "@mui/system";

export const ChangeButtonForm = ({ handleInputChange, isDisplay }) => {
    return (
        <div>
            <ButtonFormControl>
                <ButtonInput
                    name="button"
                    onChange={handleInputChange}
                    defaultValue={isDisplay.button.toUpperCase()}
                    endAdornment={<CreateOutlinedIcon position="end" />}
                />
            </ButtonFormControl>
        </div>
    );
};

const ButtonFormControl = styled(FormControl)`
    display: flex;
    align-items: center;
    text-transform: uppercase;
`;

const ButtonInput = styled(Input)`
    margin-bottom: 16px;
    align-items: center;
    padding: 6px 16px;
    width: 194px;
    height: 36px;
    background: #2196f3;
    box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.12), 0px 2px 2px rgba(0, 0, 0, 0.14),
        0px 3px 1px -2px rgba(0, 0, 0, 0.2);
    border-radius: 4px;
    text-align: center;
    font-weight: 500;
    font-size: 12px;
    line-height: calc(24 / 12);
    letter-spacing: 0.4px;
    text-transform: uppercase;
    color: #ffffff;

    &::before,
    &::after,
    &:hover::before,
    &:focus::before,
    &:hover::after,
    &:focus::after {
        border: none;
    }
`;
