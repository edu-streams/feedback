import * as React from "react";
import { TextField, Typography } from "@mui/material";
import { styled } from "@mui/system";

export const TextFieldAria = ({ handleInputChange, formData, setingForm }) => {
    return (
        <div style={{ width: 500 + "px", margin: "0 auto" }}>
            <CustomTypography variant="h6" gutterBottom>
                {setingForm["commentQuestion"]}
            </CustomTypography>

            <TextField
                onChange={handleInputChange}
                value={formData.comment}
                name="comment"
                id="standard-textarea"
                placeholder="Додатковий коментар..."
                multiline
                maxRows={4}
                variant="standard"
                style={{ width: 100 + "%" }}
            />
        </div>
    );
};

const CustomTypography = styled(Typography)`
    margin-bottom: 16px;
    font-weight: 400;
    font-size: 20px;
    line-height: calc(32 / 20);
    text-align: center;
    letter-spacing: 0.15px;
    color: rgba(0, 0, 0, 0.87);
`;
