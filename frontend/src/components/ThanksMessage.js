import * as React from "react";
import { Loader } from "./Loader/Loader";
import { Typography, Box } from "@mui/material";
import { styled } from "@mui/system";

export const ThanksMessage = ({ handleBackdropClick }) => {
    return (
        <CustomBox onClick={handleBackdropClick}>
            <CustomTypography>Дякую за зворотній зв’язок!</CustomTypography>
            <Loader />
        </CustomBox>
    );
};

const CustomBox = styled(Box)`
    padding: 8px;
    margin: 0 auto;
    position: relative;
    width: 400px;
    background-color: #fff;
    transition: transform 250ms cubic-bezier(0.4, 0, 0.2, 1);
`;

const CustomTypography = styled(Typography)`
    font-size: 16px;
    line-height: calc(24 / 16);
    text-align: center;
    letter-spacing: 0.15px;
    color: rgba(0, 0, 0, 0.87);
`;
