import * as React from "react";
import { Button } from "@mui/material";
import { styled } from "@mui/system";

export const FormButton = ({ buttonDisabled, setingForm }) => {
    return (
        <div>
            <CustomButton
                type="submit"
                variant="contained"
                disabled={buttonDisabled ? true : false}
            >
                {setingForm.button}
            </CustomButton>
        </div>
    );
};

const CustomButton = styled(Button)`
    margin: 0 auto;
    display: block;
    margin-top: 24px;
    width: 500px;
    background: #2196f3;
    box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.12), 0px 2px 2px rgba(0, 0, 0, 0.14),
        0px 3px 1px -2px rgba(0, 0, 0, 0.2);
    border-radius: 4px;
    font-weight: 500;
    font-size: 14px;
    line-height: calc(24 / 12);
    letter-spacing: 0.4px;
    text-transform: uppercase;
`;
