import { useEffect, useState } from "react";
import { socket } from "../../services/socket";
import Typography from "@mui/material/Typography";

const OBSLayout = () => {
    const [_, setIsDisplay] = useState(true);

    useEffect(() => {
        socket.on("controlFeedback", (controlFeedback) => {
            setIsDisplay(controlFeedback);
        });
    }, []);

    return (
        <>
            <Typography>OBS Layout</Typography>
            {/* <Box display={isDisplay}>
                <div>{message}</div>
            </Box> */}
        </>
    );
};
export default OBSLayout;
