import { useEffect, useState } from "react";
import { socket } from "../../services/socket";
import { Modal } from "../../components/Modal/Modal";
import { CloseButton } from "../../components/CloseButton.js";
import { Recommend } from "../../components/Recommend";
import { FormButton } from "../../components/FormButton";
import { TextFieldAria } from "../../components/TextFieldAria";
import { RateLesson } from "../../components/RateLesson";
import { ThanksMessage } from "../../components/ThanksMessage";
import { postFeedBack } from "../../services/api";

const FeedBack = () => {
    const [setingForm, setSetingForm] = useState({});
    const [buttonDisabled, setButtonDisabled] = useState(true);
    const [showThanks, setShowThanks] = useState(false);
    const [formData, setFormData] = useState({
        rate: null,
        recommend: null,
        comment: "",
    });

    useEffect(() => {
        if (formData.rate !== null) {
            setButtonDisabled(false);
        }
    }, [formData]);

    useEffect(() => {
        socket.on("controlFeedback", (data) => {
            setSetingForm(JSON.parse(data));
        });
    }, []);

    const handleInputChange = (event) => {
        const inputValue = event.target.value;
        const inputName = event.target.name;

        setFormData((prevState) => {
            return {
                ...prevState,
                [inputName]: inputValue,
            };
        });
    };

    const handleSubmit = (event) => {
        event.preventDefault();

        postFeedBack({
            userID: `${Math.random()}`,
            sessionID: `${Math.random()}`,
            ...formData,
        });

        setFormData({ rate: null, recommend: null, comment: "" });

        setSetingForm((prevState) => {
            return {
                ...prevState,
                visible: false,
            };
        });

        setShowThanks(true);

        setTimeout(() => {
            setShowThanks(false);
        }, 2500);
    };

    const handleBackdropClick = (event) => {
        if (event.currentTarget === event.target) {
            setShowThanks(false);
        }
    };

    const closeForm = () => {
        postFeedBack({
            userID: `${Math.random()}`,
            sessionID: `${Math.random()}`,
        });

        setSetingForm((prevState) => {
            return {
                ...prevState,
                visible: false,
            };
        });
    };

    return (
        <div>
            {setingForm.visible && (
                <Modal>
                    <CloseButton closeForm={closeForm} />
                    <form onSubmit={handleSubmit}>
                        <RateLesson
                            handleInputChange={handleInputChange}
                            formData={formData}
                            setingForm={setingForm}
                        />

                        <Recommend
                            formData={formData}
                            handleInputChange={handleInputChange}
                            setingForm={setingForm}
                        />

                        <TextFieldAria
                            handleInputChange={handleInputChange}
                            formData={formData}
                            setingForm={setingForm}
                        />
                        <FormButton
                            buttonDisabled={buttonDisabled}
                            setingForm={setingForm}
                        />
                    </form>
                </Modal>
            )}
            {showThanks && (
                <ThanksMessage handleBackdropClick={handleBackdropClick} />
            )}
        </div>
    );
};
export default FeedBack;
