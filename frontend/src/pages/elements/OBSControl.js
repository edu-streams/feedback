import {
    Box,
    ToggleButton,
    ToggleButtonGroup,
    Typography,
} from "@mui/material";
import { useState } from "react";
import { socket } from "../../services/socket";
import { styled } from "@mui/system";
import { QuestionGeneral } from "../../components/QuestionGeneral";
import { ChooseFormRate } from "../../components/ChooseFormRate";
import { QuestionReccomend } from "../../components/QuestionReccomend";
import { QuestionComment } from "../../components/QuestionComment";
import { ChangeButtonForm } from "../../components/ChangeButtonForm";
import { SavedButton } from "../../components/SavedButton";

const OBSControl = () => {
    const [isDisplay, setIsDisplay] = useState({
        visible: true,
        isSmile: "smile",
        question: "Твоє загальне враження від лекції?",
        commentQuestion:
            "Залиш свій коментар, зауваження або побажання щодо лекції або викладача",
        recommendQuestion: "Порадиш лекцію своєму другу?",
        button: "Відправити оцінку",
    });
    const [showSettings, setShowSettings] = useState(false);

    const handleChange = (event) => {
        if (event.target.value === isDisplay.visible.toString()) return;

        socket.emit("controlFeedback", JSON.stringify(isDisplay));
        setIsDisplay((s) => ({ ...isDisplay, visible: !s.visible }));
    };

    const handleInputChange = (event) => {
        const inputValue = event.target.value;
        const inputName = event.target.name;

        setIsDisplay((prevState) => {
            return {
                ...prevState,
                [inputName]: inputValue,
            };
        });
    };

    const chooseRate = (_, newValue) => {
        setIsDisplay((prevState) => {
            return {
                ...prevState,
                isSmile: newValue,
            };
        });
    };

    return (
        <>
            <Typography>OBS Control</Typography>

            <ToggleButtonGroup
                color="primary"
                value={isDisplay.visible}
                exclusive
                onChange={handleChange}
                aria-label="Platform"
                style={{ marginBottom: 15 + "px" }}
            >
                <ToggleButton value={false}>Відправити форму</ToggleButton>
                <ToggleButton value={true}>Сховати форму</ToggleButton>
            </ToggleButtonGroup>

            <ToggleButtonGroup
                color="primary"
                value={showSettings}
                exclusive
                onChange={() => {
                    setShowSettings(!showSettings);
                }}
                aria-label="Platform"
            >
                <ToggleButton value="false">Налаштування</ToggleButton>
            </ToggleButtonGroup>

            {showSettings && (
                <SettingForm>
                    <Title>Налаштування оцінювання</Title>

                    <QuestionGeneral
                        handleInputChange={handleInputChange}
                        isDisplay={isDisplay}
                    />

                    <ChooseFormRate
                        isDisplay={isDisplay}
                        chooseRate={chooseRate}
                    />

                    <QuestionReccomend
                        handleInputChange={handleInputChange}
                        isDisplay={isDisplay}
                    />

                    <QuestionComment
                        handleInputChange={handleInputChange}
                        isDisplay={isDisplay}
                    />

                    <ChangeButtonForm
                        handleInputChange={handleInputChange}
                        isDisplay={isDisplay}
                    />

                    <SavedButton
                        setShowSettings={setShowSettings}
                        showSettings={showSettings}
                    />
                </SettingForm>
            )}
        </>
    );
};

export default OBSControl;

const SettingForm = styled(Box)`
    width: 345px;
    padding: 8px;
    margin-top: 16px;
    background-color: #f8f8f8;
`;

const Title = styled(Typography)`
    margin-bottom: 16px;
    font-size: 20px;
    line-height: calc(29 / 20);
    letter-spacing: 0.17px;
    color: #000000;
    text-align: center;
`;
