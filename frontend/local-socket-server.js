const express = require("express");
const app = express();
const http = require("http").Server(app);

const io = require("socket.io")(http, {
    path: "/event-server",
    cors: ["http://localhost:3000/"],
});

app.use(express.static("public"));

let isForwardMessages = "true";
io.on("connection", (socket) => {
    console.log("User connected.");

    socket.on("disconnect", () => {
        console.log("User disconnected.");
    });

    socket.onAny((e, data) => {
        console.log(`${isForwardMessages} -> ${e}: ${data}`);

        socket.broadcast.emit(e, data);
    });
});

const port = process.env.PORT || 4000;
http.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
