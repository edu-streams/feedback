const { feedbackController } = require('../controllers');
// const gradeController = require('../controllers/grade.controller');

const router = require('express').Router();

router.post('/',
    feedbackController.postFeedback);
router.get('/',
    feedbackController.getAllFeedback);
router.get('/:id',
    feedbackController.getFeedbackBySesionID);


module.exports = router;