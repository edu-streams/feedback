You need the latest npm and node installed on your machine.
* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

_To build app you need to do the following._

1. Install NPM packages
   ```sh
   npm install
   ```
3. Launch server.Server will be running on port 8080
   ```sh
   npm run start:dev
   ```
4. Launch Docker
   ```sh
   npm run compose
   ```
5. Migrate
   ```sh
   npm run migrate
   ```
   