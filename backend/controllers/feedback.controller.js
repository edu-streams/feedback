const feedbackService = require("../service/feedback.service");

module.exports = {
  postFeedback: async (req, res, next) => {
    try {
      // const { userName, grade, comment } = req.body;
      console.log(req.body);

      const newFeedback = await feedbackService.createFeedback(req.body);

      res.status(200).json(newFeedback);
    } catch (error) {
      next(error);
    }
  },
  getAllFeedback: async (req, res, next) => {
    try {
      const allFeedbacks = await feedbackService.getAll();

      res.status(201).json(allFeedbacks);
    } catch (error) {
      next(error);
    }
  },
  getFeedbackBySesionID: async (req, res, next) => {
    try {
      const sessionId = req.params.id;
      const allFeedbacks = await feedbackService.findBySessionId(sessionId);

      res.status(201).json(allFeedbacks);
    } catch (error) {
      next(error);
    }
  },
};
