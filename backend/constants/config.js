module.exports = {
    PORT: process.env.PORT || 8080,
    CORS_WHITE_LIST: process.env.CORS_WHITE_LIST || 'http://localhost:3000',
    // MONGO_URL: process.env.MONGO_URL || 'mongodb://localhost:27017/grades',
};