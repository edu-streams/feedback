'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Feedback extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
    toJSON() {
      return { ...this.get(), id: undefined };
    }
  }
  Feedback.init(
    {
      uuid: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      userID: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          isString(value) {
            if (typeof value !== 'string') {
              throw new Error('userID must be a string');
            }
          },
        },
      },
      sessionID: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
          isString(value) {
            if (typeof value !== 'string') {
              throw new Error('sessionID must be a string');
            }
          },
        },
      },
      rate: {
        type: DataTypes.INTEGER,
        allowNull: true,
        validate: {
          isInt: true,
          min: 1,
          max: 5,
        },
      },
      recommend: {
        type: DataTypes.INTEGER,
        allowNull: true,
        validate: {
          isInt: true,
          min: 1,
          max: 10,
        },
      },
      comment: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          len: [0, 255],
        },
      },
    }, {
    sequelize,
    tableName: 'feedback',
    modelName: 'Feedback',
  });
  return Feedback;
};