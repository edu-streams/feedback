const express = require("express");
// const mongoose = require('mongoose');
const cors = require("cors");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");

const { PORT, CORS_WHITE_LIST } = require("./constants/config");
const { feedbackRouter } = require("./routes");
const db = require("./dataBase/models");

// mongoose.connect(MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true });
// const db = mongoose.connection;
// db.on('error', console.error.bind(console, 'MongoDB connection error:'));
// db.once('open', () => {
//     console.log('MongoDB database connection established successfully');
// });

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  cors(_configureCors())
);

app.get("/", (req, res) => {
  res.json("Hello"); // Send a "Hello" message to the client
});

app.use("/feedback/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use("/feedback/api/", feedbackRouter);

app.use("*", (req, res) => {
  res.status(404).json("page not found");
});

app.use((err, req, res, next) => {
  res.status(err.status || 500).json({
    error: err.message || "Unknown error",
  });
});

db.sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
    app.listen(PORT, () => {
      console.log(`Server listening on port ${PORT}`);
    });
  })
  .catch((error) => {
    console.error("Unable to connect to the database:", error);
  });

function _configureCors() {
  const whitelist = [...CORS_WHITE_LIST.split(';'), `http://localhost:${PORT}`];



  return {

    origin: (origin, callback) => {
      console.log("origin", origin);

      if (!origin || whitelist.includes(origin)) {
        return callback(null, true);
      }

      callback(new Error('Not allowed by CORS'));
    }
  };
}
